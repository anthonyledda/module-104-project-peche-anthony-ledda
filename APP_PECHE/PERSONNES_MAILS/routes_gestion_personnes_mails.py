# routes_gestion_personnes_mails.py
# OM 2020.04.16 Gestions des "routes" FLASK pour la table intermédiaire qui associe les personnes et les mails.

from flask import render_template, request, flash, session
from APP_PECHE import app
from APP_PECHE.MAILS.data_gestion_mails import GestionMails
from APP_PECHE.PERSONNES_MAILS.data_gestion_personnes_mails import GestionPersonnesMails


# ---------------------------------------------------------------------------------------------------
# OM 2020.04.26 Définition d'une "route" /personnes_mails_afficher_concat
# Récupère la liste de tous les personnes et de tous les mails associés aux personnes.
# ---------------------------------------------------------------------------------------------------
@app.route("/personnes_mails_afficher_concat/<int:id_personne_sel>", methods=['GET', 'POST'])
def personnes_mails_afficher_concat (id_personne_sel):
    print("id_personne_sel ", id_personne_sel)
    if request.method == "GET":
        try:
            # OM 2020.04.09 Objet contenant toutes les méthodes pour gérer (CRUD) les données.
            obj_actions_mails = GestionPersonnesMails()
            # Récupère les données grâce à une requête MySql définie dans la classe GestionMails()
            # Fichier data_gestion_mails.py
            data_pers_a_mails_afficher_concat = obj_actions_mails.pers_a_mails_afficher_data_concat(id_personne_sel)
            # DEBUG bon marché : Pour afficher le résultat et son type.
            print(" data mails", data_pers_a_mails_afficher_concat, "type ", type(data_pers_a_mails_afficher_concat))

            # Différencier les messages si la table est vide.
            if data_pers_a_mails_afficher_concat:
                # OM 2020.04.09 La ligne ci-dessous permet de donner un sentiment rassurant aux utilisateurs.
                flash(f"Données mails affichés dans PersAMail!!", "success")
            else:
                flash(f"""Le personne demandé n'existe pas. Ou la table "t_pers_a_mails" est vide. !!""", "warning")
        except Exception as erreur:
            print(f"RGGF Erreur générale.")
            # OM 2020.04.09 On dérive "Exception" par le "@app.errorhandler(404)" fichier "run_mon_app.py"
            # Ainsi on peut avoir un message d'erreur personnalisé.
            # flash(f"RGG Exception {erreur}")
            raise Exception(f"RGGF Erreur générale. {erreur}")
            # raise MaBdErreurOperation(f"RGG Exception {msg_erreurs['ErreurNomBD']['message']} {erreur}")

    # OM 2020.04.21 Envoie la page "HTML" au serveur.
    return render_template("personnes_mails/personnes_mails_afficher.html",
                           data=data_pers_a_mails_afficher_concat)


# ---------------------------------------------------------------------------------------------------
# OM 2020.04.21 Définition d'une "route" /gf_edit_personne_mail_selected
# Récupère la liste de tous les mails du personne sélectionné.
# Nécessaire pour afficher tous les "TAGS" des mails, ainsi l'utilisateur voit les mails à disposition
# ---------------------------------------------------------------------------------------------------
@app.route("/gf_edit_personne_mail_selected", methods=['GET', 'POST'])
def gf_edit_personne_mail_selected ():
    if request.method == "GET":
        try:

            # OM 2020.04.09 Objet contenant toutes les méthodes pour gérer (CRUD) les données.
            obj_actions_mails = GestionMails()
            # Récupère les données grâce à une requête MySql définie dans la classe GestionMails()
            # Fichier data_gestion_mails.py
            # Pour savoir si la table "t_mails" est vide, ainsi on empêche l’affichage des tags
            # dans le render_template(personnes_mails_modifier_tags_dropbox.html)
            data_mails_all = obj_actions_mails.mails_afficher_data('ASC', 0)

            # OM 2020.04.09 Objet contenant toutes les méthodes pour gérer (CRUD) les données de la table intermédiaire.
            obj_actions_mails = GestionPersonnesMails()

            # OM 2020.04.21 Récupère la valeur de "id_Personne" du formulaire html "personnes_mails_afficher.html"
            # l'utilisateur clique sur le lien "Modifier mails de ce personne" et on récupère la valeur de "id_Personne" grâce à la variable "id_personne_mail_edit_html"
            # <a href="{{ url_for('gf_edit_personne_mail_selected', id_personne_mail_edit_html=row.id_Personne) }}">Modifier les mails de ce personne</a>
            id_personne_mail_edit = request.values['id_personne_mail_edit_html']

            # OM 2020.04.21 Mémorise l'id du personne dans une variable de session
            # (ici la sécurité de l'application n'est pas engagée)
            # il faut éviter de stocker des données sensibles dans des variables de sessions.
            session['session_id_personne_mail_edit'] = id_personne_mail_edit

            # Constitution d'un dictionnaire pour associer l'id du personne sélectionné avec un nom de variable
            valeur_id_personne_selected_dictionnaire = {"value_id_personne_selected": id_personne_mail_edit}

            # Récupère les données grâce à 3 requêtes MySql définie dans la classe GestionPersonnesMails()
            # 1) Sélection du personne choisi
            # 2) Sélection des mails "déjà" attribués pour le personne.
            # 3) Sélection des mails "pas encore" attribués pour le personne choisi.
            # Fichier data_gestion_personnes_mails.py
            # ATTENTION à l'ordre d'assignation des variables retournées par la fonction "pers_a_mails_afficher_data"
            data_pers_a_mail_selected, data_pers_a_mails_non_attribues, data_pers_a_mails_attribues = \
                obj_actions_mails.pers_a_mails_afficher_data(valeur_id_personne_selected_dictionnaire)

            lst_data_personne_selected = [item['id_Personne'] for item in data_pers_a_mail_selected]
            # Pour afficher le résultat et son type.
            print("lst_data_personne_selected  ", lst_data_personne_selected,
                  type(lst_data_personne_selected))

            # Dans le composant "tags-selector-tagselect" on doit connaître
            # les mails qui ne sont pas encore sélectionnés.
            lst_data_pers_a_mails_non_attribues = [item['id_Mail'] for item in data_pers_a_mails_non_attribues]
            session['session_lst_data_pers_a_mails_non_attribues'] = lst_data_pers_a_mails_non_attribues
            # Pour afficher le résultat et son type.
            print("lst_data_pers_a_mails_non_attribues  ", lst_data_pers_a_mails_non_attribues,
                  type(lst_data_pers_a_mails_non_attribues))

            # Dans le composant "tags-selector-tagselect" on doit connaître
            # les mails qui sont déjà sélectionnés.
            lst_data_pers_a_mails_old_attribues = [item['id_Mail'] for item in data_pers_a_mails_attribues]
            session['session_lst_data_pers_a_mails_old_attribues'] = lst_data_pers_a_mails_old_attribues
            # Pour afficher le résultat et son type.
            print("lst_data_pers_a_mails_old_attribues  ", lst_data_pers_a_mails_old_attribues,
                  type(lst_data_pers_a_mails_old_attribues))

            # Pour afficher le résultat et son type.
            print(" data data_pers_a_mail_selected", data_pers_a_mail_selected, "type ", type(data_pers_a_mail_selected))
            print(" data data_pers_a_mails_non_attribues ", data_pers_a_mails_non_attribues, "type ",
                  type(data_pers_a_mails_non_attribues))
            print(" data_pers_a_mails_attribues ", data_pers_a_mails_attribues, "type ",
                  type(data_pers_a_mails_attribues))

            # Extrait les valeurs contenues dans la table "t_mails", colonne "NomMail"
            # Le composant javascript "tagify" pour afficher les tags n'a pas besoin de l'id_Mail
            lst_data_pers_a_mails_non_attribues = [item['NomMail'] for item in data_pers_a_mails_non_attribues]
            # Pour afficher le résultat et son type.
            print("lst_all_mails gf_edit_personne_mail_selected ", lst_data_pers_a_mails_non_attribues,
                  type(lst_data_pers_a_mails_non_attribues))

            # Différencier les messages si la table est vide.
            if lst_data_personne_selected == [None]:
                flash(f"""Le personne demandé n'existe pas. Ou la table "t_pers_a_mails" est vide. !!""", "warning")
            else:
                # OM 2020.04.09 La ligne ci-dessous permet de donner un sentiment rassurant aux utilisateurs.
                flash(f"Données mails affichées dans PersAMail!!", "success")

        except Exception as erreur:
            print(f"RGGF Erreur générale.")
            # OM 2020.04.09 On dérive "Exception" par le "@app.errorhandler(404)"
            # fichier "run_mon_app.py"
            # Ainsi on peut avoir un message d'erreur personnalisé.
            # flash(f"RGG Exception {erreur}")
            raise Exception(f"RGGF Erreur générale. {erreur}")
            # raise MaBdErreurOperation(f"RGG Exception {msg_erreurs['ErreurNomBD']['message']} {erreur}")

    # OM 2020.04.21 Envoie la page "HTML" au serveur.
    return render_template("personnes_mails/personnes_mails_modifier_tags_dropbox.html",
                           data_mails=data_mails_all,
                           data_personne_selected=data_pers_a_mail_selected,
                           data_mails_attribues=data_pers_a_mails_attribues,
                           data_mails_non_attribues=data_pers_a_mails_non_attribues)


# ---------------------------------------------------------------------------------------------------
# OM 2020.04.26 Définition d'une "route" /gf_update_personne_mail_selected
# Récupère la liste de tous les mails du personne sélectionné.
# Nécessaire pour afficher tous les "TAGS" des mails, ainsi l'utilisateur voit les mails à disposition
# ---------------------------------------------------------------------------------------------------
@app.route("/gf_update_personne_mail_selected", methods=['GET', 'POST'])
def gf_update_personne_mail_selected ():
    if request.method == "POST":
        try:
            # Récupère l'id du personne sélectionné
            id_personne_selected = session['session_id_personne_mail_edit']
            print("session['session_id_personne_mail_edit'] ", session['session_id_personne_mail_edit'])

            # Récupère la liste des mails qui ne sont pas associés au personne sélectionné.
            old_lst_data_pers_a_mails_non_attribues = session['session_lst_data_pers_a_mails_non_attribues']
            print("old_lst_data_pers_a_mails_non_attribues ", old_lst_data_pers_a_mails_non_attribues)

            # Récupère la liste des mails qui sont associés au personne sélectionné.
            old_lst_data_pers_a_mails_attribues = session['session_lst_data_pers_a_mails_old_attribues']
            print("old_lst_data_pers_a_mails_old_attribues ", old_lst_data_pers_a_mails_attribues)

            # Effacer toutes les variables de session.
            session.clear()

            # Récupère ce que l'utilisateur veut modifier comme mails dans le composant "tags-selector-tagselect"
            # dans le fichier "personnes_mails_modifier_tags_dropbox.html"
            new_lst_str_pers_a_mails = request.form.getlist('name_select_tags')
            print("new_lst_str_pers_a_mails ", new_lst_str_pers_a_mails)

            # OM 2020.04.29 Dans "name_select_tags" il y a ['4','65','2']
            # On transforme en une liste de valeurs numériques. [4,65,2]
            new_lst_int_pers_a_mails_old = list(map(int, new_lst_str_pers_a_mails))
            print("new_lst_pers_a_mails ", new_lst_int_pers_a_mails_old, "type new_lst_pers_a_mails ",
                  type(new_lst_int_pers_a_mails_old))

            # Pour apprécier la facilité de la vie en Python... "les ensembles en Python"
            # https://fr.wikibooks.org/wiki/Programmation_Python/Ensembles
            # OM 2020.04.29 Une liste de "id_Mail" qui doivent être effacés de la table intermédiaire "t_pers_a_mails".
            lst_diff_mails_delete_b = list(
                set(old_lst_data_pers_a_mails_attribues) - set(new_lst_int_pers_a_mails_old))
            # Pour afficher le résultat de la liste.
            print("lst_diff_mails_delete_b ", lst_diff_mails_delete_b)

            # OM 2020.04.29 Une liste de "id_Mail" qui doivent être ajoutés à la BD
            lst_diff_mails_insert_a = list(
                set(new_lst_int_pers_a_mails_old) - set(old_lst_data_pers_a_mails_attribues))
            # Pour afficher le résultat de la liste.
            print("lst_diff_mails_insert_a ", lst_diff_mails_insert_a)

            # OM 2020.04.09 Objet contenant toutes les méthodes pour gérer (CRUD) les données.
            obj_actions_mails = GestionPersonnesMails()

            # Pour le personne sélectionné, parcourir la liste des mails à INSÉRER dans la "t_pers_a_mails".
            # Si la liste est vide, la boucle n'est pas parcourue.
            for id_Mail_ins in lst_diff_mails_insert_a:
                # Constitution d'un dictionnaire pour associer l'id du personne sélectionné avec un nom de variable
                # et "id_Mail_ins" (l'id du mail dans la liste) associé à une variable.
                valeurs_personne_sel_mail_sel_dictionnaire = {"value_fk_Personne": id_personne_selected,
                                                           "value_fk_Mail": id_Mail_ins}
                # Insérer une association entre un(des) mail(s) et le personne sélectionner.
                obj_actions_mails.pers_a_mails_add(valeurs_personne_sel_mail_sel_dictionnaire)

            # Pour le personne sélectionné, parcourir la liste des mails à EFFACER dans la "t_pers_a_mails".
            # Si la liste est vide, la boucle n'est pas parcourue.
            for id_Mail_del in lst_diff_mails_delete_b:
                # Constitution d'un dictionnaire pour associer l'id du personne sélectionné avec un nom de variable
                # et "id_Mail_del" (l'id du mail dans la liste) associé à une variable.
                valeurs_personne_sel_mail_sel_dictionnaire = {"value_fk_Personne": id_personne_selected,
                                                           "value_fk_Mail": id_Mail_del}
                # Effacer une association entre un(des) mail(s) et le personne sélectionner.
                obj_actions_mails.pers_a_mails_delete(valeurs_personne_sel_mail_sel_dictionnaire)

            # Récupère les données grâce à une requête MySql définie dans la classe GestionMails()
            # Fichier data_gestion_mails.py
            # Afficher seulement le personne dont les mails sont modifiés, ainsi l'utilisateur voit directement
            # les changements qu'il a demandés.
            data_pers_a_mails_afficher_concat = obj_actions_mails.pers_a_mails_afficher_data_concat(id_personne_selected)
            # Pour afficher le résultat et son type.
            print(" data mails", data_pers_a_mails_afficher_concat, "type ", type(data_pers_a_mails_afficher_concat))

            # Différencier les messages si la table est vide.
            if data_pers_a_mails_afficher_concat == None:
                flash(f"""Le personne demandé n'existe pas. Ou la table "t_pers_a_mails" est vide. !!""", "warning")
            else:
                # OM 2020.04.09 La ligne ci-dessous permet de donner un sentiment rassurant aux utilisateurs.
                flash(f"Données mails affichées dans PersAMail!!", "success")

        except Exception as erreur:
            print(f"RGGF Erreur générale.")
            # OM 2020.04.09 On dérive "Exception" par le "@app.errorhandler(404)" fichier "run_mon_app.py"
            # Ainsi on peut avoir un message d'erreur personnalisé.
            # flash(f"RGG Exception {erreur}")
            raise Exception(f"RGGF Erreur générale. {erreur}")
            # raise MaBdErreurOperation(f"RGG Exception {msg_erreurs['ErreurNomBD']['message']} {erreur}")

    # Après cette mise à jour de la table intermédiaire "t_pers_a_mails",
    # on affiche les personnes et le(urs) mail(s) associé(s).
    return render_template("personnes_mails/personnes_mails_afficher.html",
                           data=data_pers_a_mails_afficher_concat)
