# routes.py
# OM 2020.04.02 Pour faire des tests divers et variés.
import flask
from flask import render_template, session, request, flash
from flask_login import login_user

from APP_PECHE import app
from APP_PECHE.DATABASE.erreurs import *


@app.route('/index')
def index():
    return "Hello, World!"

# AL 2020.05.15 Pour faire un système de login
# Code repris de la documentation FLASK
# https://pythonspot.com/login-authentication-with-flask/
from APP_PECHE.DATABASE.connect_db_context_manager import MaBaseDeDonnee


@app.route('/')
def home():
    # if not session.get('logged_in'):
    #     return render_template('login.html')
    # else:
    return render_template('home.html')

@app.route("/logout")
def logout():
    session['logged_in'] = False
    return home()