# data_gestion_especes.py
# AL 2020.05.12 Permet de gérer (CRUD) les données de la table t_especes
from flask import flash

from APP_PECHE.DATABASE.connect_db_context_manager import MaBaseDeDonnee
from APP_PECHE.DATABASE.erreurs import *


class GestionEspeces:
    def __init__(self):
        try:
            # Affichage des données dans la console
            print("dans le try de gestions especes")
            # AL 2020.05.12 La connexion à la base de données est-elle active ?
            # Renvoie une erreur si la connexion est perdue.
            MaBaseDeDonnee().connexion_bd.ping(False)
        except Exception as erreur:
            flash("Il faut connecter une base de donnée", "Danger")
            print(f"Exception grave Classe constructeur GestionEspeces {erreur.args[0]}")
            # Ainsi on peut avoir un message d'erreur personnalisé.
            raise MaBdErreurConnexion(f"{msg_erreurs['ErreurConnexionBD']['message']} {erreur.args[0]}")
        print("Classe constructeur GestionEspeces ")

    def especes_afficher_data(self):
        try:
            # la commande MySql classique est "SELECT * FROM t_especes"
            strsql_especes_afficher = """SELECT id_Espece, ThumbnailEspeceLink, NomEspece, 
            NomScientifiqueEspece, DescriptionEspece, PoidsMaxEspece, LongMaxEspece, AgeMaxEspece, ProfEspece, PlanEauEspece FROM t_especes WHERE ThumbnailEspeceLink IS NOT NULL ORDER BY NomEspece ASC"""

            # Du fait de l'utilisation des "context managers" on accède au curseur grâce au "with".
            with MaBaseDeDonnee().connexion_bd.cursor() as mc_afficher:
                # Envoi de la commande MySql
                mc_afficher.execute(strsql_especes_afficher)
                # Récupère les données de la requête.
                data_especes = mc_afficher.fetchall()
                # Affichage dans la console
                print("data_especes ", data_especes, " Type : ", type(data_especes))
                # Retourne les données du "SELECT"
                return data_especes
        except pymysql.Error as erreur:
            print(f"DGG gad pymysql errror {erreur.args[0]} {erreur.args[1]}")
            raise MaBdErreurPyMySl(
                f"DGG gad pymysql errror {msg_erreurs['ErreurPyMySql']['message']} {erreur.args[0]} {erreur.args[1]}")
        except Exception as erreur:
            print(f"DGG gad Exception {erreur.args}")
            raise MaBdErreurConnexion(f"DGG gad Exception {msg_erreurs['ErreurConnexionBD']['message']} {erreur.args}")
        except pymysql.err.IntegrityError as erreur:
            # AL 2020.05.12 On dérive "pymysql.err.IntegrityError" dans le fichier "erreurs.py"
            # Ainsi on peut avoir un message d'erreur personnalisé.
            raise MaBdErreurConnexion(f"DGG gad pei {msg_erreurs['ErreurConnexionBD']['message']} {erreur.args[1]}")