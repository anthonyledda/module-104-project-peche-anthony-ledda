# routes_gestion_profil.py
# AL 2020.05.06 profil des "routes" FLASK pour les profil.


from flask import render_template, flash, redirect, url_for, request
from APP_PECHE import app
from APP_PECHE.PROFIL.data_gestion_profil import GestionProfil
from APP_PECHE.DATABASE.erreurs import *
import os
