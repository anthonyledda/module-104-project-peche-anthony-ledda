# data_gestion_profil.py
# AL 2020.05.12 Permet de gérer (CRUD) les données de la table t_profil
from flask import flash

from APP_PECHE.DATABASE.connect_db_context_manager import MaBaseDeDonnee
from APP_PECHE.DATABASE.erreurs import *


class GestionProfil:
    def __init__(self):
        try:
            # DEBUG bidon dans la console
            print("dans le try de gestions profil")
            # AL 2020.05.12 La connexion à la base de données est-elle active ?
            # Renvoie une erreur si la connexion est perdue.
            MaBaseDeDonnee().connexion_bd.ping(False)
        except Exception as erreur:
            flash("Il faut connecter une base de donnée", "Danger")
            # DEBUG bidon : Pour afficher un message dans la console.
            print(f"Exception grave Classe constructeur GestionProfil {erreur.args[0]}")
            # Ainsi on peut avoir un message d'erreur personnalisé.
            raise MaBdErreurConnexion(f"{msg_erreurs['ErreurConnexionBD']['message']} {erreur.args[0]}")
        print("Classe constructeur GestionProfil ")

    def profil_afficher_data (self, valeur_id_current_profil_dict):
        print("valeur_id_current_profil_dict...", valeur_id_current_profil_dict)
        try:

            # OM 2020.04.07 C'EST LA QUE VOUS ALLEZ DEVOIR PLACER VOTRE PROPRE LOGIQUE MySql
            # la commande MySql classique est "SELECT * FROM t_mails"
            # Pour "lever"(raise) une erreur s'il y a des erreurs sur les noms d'attributs dans la table
            # donc, je précise les champs à afficher

            strsql_personne_selected = """SELECT id_Personne, NomPers, PrenomPers, DateNaissPers, User, PhotoProfil FROM t_personnes AS T1
                                        INNER JOIN t_login AS T2 ON T2.id_Login = T1.fk_Login"""

            # Du fait de l'utilisation des "context managers" on accède au curseur grâce au "with".
            with MaBaseDeDonnee().connexion_bd.cursor() as mc_afficher:

                # Envoi de la commande MySql
                mc_afficher.execute(strsql_personne_selected, valeur_id_current_profil_dict)
                # Récupère les données de la requête.
                data_personne_selected = mc_afficher.fetchall()
                # Affichage dans la console
                print("data_personne_selected  ", data_personne_selected, " Type : ", type(data_personne_selected))

                # Retourne les données du "SELECT"
                return data_personne_selected
        except pymysql.Error as erreur:
            print(f"DGGF gfad pymysql errror {erreur.args[0]} {erreur.args[1]}")
            raise MaBdErreurPyMySl(
                f"DGG gad pymysql errror {msg_erreurs['ErreurPyMySql']['message']} {erreur.args[0]} {erreur.args[1]}")
        except Exception as erreur:
            print(f"DGGF gfad Exception {erreur.args}")
            raise MaBdErreurConnexion(f"DGG gad Exception {msg_erreurs['ErreurConnexionBD']['message']} {erreur.args}")
        except pymysql.err.IntegrityError as erreur:
            # OM 2020.04.09 On dérive "pymysql.err.IntegrityError" dans le fichier "erreurs.py"
            # Ainsi on peut avoir un message d'erreur personnalisé.
            raise MaBdErreurConnexion(f"DGGF gfad pei {msg_erreurs['ErreurConnexionBD']['message']} {erreur.args[1]}")

    def profil_afficher_data_concat (self, id_current_profil):
        print("id_current_profil  ", id_current_profil)
        try:
            # OM 2020.04.07 C'EST LA QUE VOUS ALLEZ DEVOIR PLACER VOTRE PROPRE LOGIQUE MySql
            # la commande MySql classique est "SELECT * FROM t_mails"
            # Pour "lever"(raise) une erreur s'il y a des erreurs sur les noms d'attributs dans la table
            # donc, je précise les champs à afficher

            strsql_profil_afficher_data_concat = """SELECT id_Personne, NomPers, PrenomPers, DateNaissPers,
                                                            GROUP_CONCAT(NomMail) as PersAMail FROM t_id_personne_selected AS T1
                                                            RIGHT JOIN t_personnes AS T2 ON T2.id_Personne = T1.fk_Personne
                                                            LEFT JOIN t_mails AS T3 ON T3.id_Mail = T1.fk_Mail
                                                            GROUP BY id_Personne"""

            # Du fait de l'utilisation des "context managers" on accède au curseur grâce au "with".
            with MaBaseDeDonnee().connexion_bd.cursor() as mc_afficher:
                # le paramètre 0 permet d'afficher tous les personnes
                # Sinon le paramètre représente la valeur de l'id du personne
                if id_current_profil == 0:
                    mc_afficher.execute(strsql_profil_afficher_data_concat)
                else:
                    # Constitution d'un dictionnaire pour associer l'id du personne sélectionné avec un nom de variable
                    valeur_id_current_profil_dictionnaire = {"value_id_current_profil": id_current_profil}
                    strsql_profil_afficher_data_concat += """ HAVING id_Personne= %(value_id_current_profil)s"""
                    # Envoi de la commande MySql
                    mc_afficher.execute(strsql_profil_afficher_data_concat, valeur_id_current_profil_dictionnaire)

                # Récupère les données de la requête.
                data_id_personne_selected_afficher_concat = mc_afficher.fetchall()
                # Affichage dans la console
                print("dggf data_id_personne_selected_afficher_concat ", data_id_personne_selected_afficher_concat, " Type : ",
                      type(data_id_personne_selected_afficher_concat))

                # Retourne les données du "SELECT"
                return data_id_personne_selected_afficher_concat


        except pymysql.Error as erreur:
            print(f"DGGF gfadc pymysql errror {erreur.args[0]} {erreur.args[1]}")
            raise MaBdErreurPyMySl(
                f"DGG gad pymysql errror {msg_erreurs['ErreurPyMySql']['message']} {erreur.args[0]} {erreur.args[1]}")
        except Exception as erreur:
            print(f"DGGF gfadc Exception {erreur.args}")
            raise MaBdErreurConnexion(
                f"DGG gfadc Exception {msg_erreurs['ErreurConnexionBD']['message']} {erreur.args}")
        except pymysql.err.IntegrityError as erreur:
            # OM 2020.04.09 On dérive "pymysql.err.IntegrityError" dans le fichier "erreurs.py"
            # Ainsi on peut avoir un message d'erreur personnalisé.
            raise MaBdErreurConnexion(f"DGGF gfadc pei {msg_erreurs['ErreurConnexionBD']['message']} {erreur.args[1]}")
